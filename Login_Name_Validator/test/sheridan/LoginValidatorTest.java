package sheridan;

import static org.junit.Assert.*;


import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "NilPatel" ) );
	}
	@Test
	public void testIsValidLoginNameException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1bj4hfde.uwfh" ) );
	}
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ShivaniDave" ) );
	}
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "NilP.Atel2" ) );
	}
	@Test
	public void testIsValidLoginNameRegular1( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Patelnil" ) );
	}
	@Test
	public void testIsValidLoginNameException1( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "nil" ) );
	}
	@Test
	public void testIsValidLoginBoundaryIn1( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "NilPatel" ) );
	}
	@Test
	public void testIsValidLoginBoundaryOut1( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "nilpa" ) );
	}
}
